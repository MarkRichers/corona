# File parameters

casurl = 'https://covid.ourworldindata.org/data/'

casfil = 'owid-covid-data.csv'         # case file name

datrdc = [3]                           # columns to read as dates
datcol = 'date'                        # date column
dimcol = 'iso_code'                    # dimension column (location or otherwise)
cascol = ['new_cases', 'new_deaths']   # case columns (new, not cumulative)
namcol = 'location'                    # full dimension name
popcol = 'population'                  # population column

futext = '365 days'                    # future extension of dataframe
datmrg = '0 days'                      # date margin to ignore at end

# Run parameters

dimlist = ['NLD']                      # list of dimension values to analyze

measure  = 'new_cases'                 # file column to use as measure
smeasure = 'Week window'               # smoothed measures
rmeasure = 'rcases'                    # remaining measures after iteration
pmeasure = 'Model'                     # projected measures summed
wmeasure = 'Wave '                     # wave name prefix, zero-leading number is added
wavenum  = 2                           # wave numbering width

sdays    = 7                           # number of days for smoothing window
firstwav = 1                           # initial wave number
popcases = 9e9                         # relative minimum from population size (one in n cases)
mincases = 2                           # absolute minimum number of cases to consider

linmax   = -8/20                       # upper bound of linear derivation
linmin   = -8/2                        # lower bound of linear derivation
betamax  = 80/2                          # maximum accepted beta estimate
datamin  = 3                           # minimum number of data points in spline
projmin  = 1                           # minimum cases for projected start and floor
gradmin  = 1e-6                        # minimum gradient difference for knot

# pyearth parameters

pyearth  = {'minspan'     : 1,         # minimal length of spline
            'penalty'     : 0,         # strictness in pruning
            'endspan'     : 0,         # measures allowed remaining
            'thresh'      : 1e-9,      # improvement threshold for knot
            'check_every' : 1}         # measures considered

# Plot parameters

plotshw  = True
plotmin  = '20200101'                  # start date of output plots
plotmax  = '20210701'                  # stop date of output plots

plot     = {'figsize'     : (16, 9),   # figure size
            'grid'        : True,      # show grid
            'kind'        : 'area',    # plot kind
            'stacked'     : False,     # stacking
            'alpha'       : 1/3}       # transparency

plnew    = 'Daily new cases for '      # title start for new measure plot
plcum    = 'Cumulative cases for '     # title start for cumulative measure plot

# Messages

msg_data = '--- no data left'
msg_wave = '--- no data for wave'
